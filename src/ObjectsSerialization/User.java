
package ObjectsSerialization;

/**
 * Contains guest properties, extends from player
 * 
 * @author EliavBuskila
 */
public class User extends Player {

    private String Password;
    private String Email;
    private int Code;

    public User() {
        super();
        this.Password = null;
        this.Email = null;
    }
    public User(Player player) {
        super(player);
        this.Password = null;
        this.Email = null;
    }
    public String getPassword() {
        return this.Password;
    }
    public String getEmail() {
        return this.Email;
    }
    public double getCode() {
        return this.Code;
    }
    
    public void setPassword(String Password) {
        this.Password = Password;
    }
    public void setEmail(String mail) {
        this.Email = mail;
    }
    public void setCode(int Code) {
        this.Code = Code;
    }
    
    public double createCode() {
        this.Code = (int)((Math.random()*100)%10000)+1000;
        return this.Code;
    }
    public boolean isTrueCode(double Code) {
        return (this.Code == Code);
    }
    
    @Override
    public String toString() {
        return super.toString() +
                ", Password: " + this.Password + 
                ", Mail: " + this.Email +
                ", Code: " + this.Code;
    }
}
