
package Enums;

/**
 * All requests that the client can request from the server,
 * next to each request is also the way to call a database procedure.
 * 
 * @author EliavBuskila
 */
public enum Request {
    
    ADD_USER("add_user ?,?,?"),
    SEND_CODE(""),
    USER_LOGIN("add_user_login ?,?"),
    GUEST_LOGIN("add_guest_login ?"),
    GET_STATISTIC("get_statistic ?"),
    GET_ALL_BOARDS("get_all_boards ?"),
    RANDOM_BOARD(""),
    ADD_BOARD("add_board ?,?"),
    GET_GAME("get_game ?,?"),
    CHECK_SOLUTION(""),
    SOLVE_BOARD(""),
    UPDATE_GAME("update_game ?,?,?,?,?"),
    UPDATE_USERNAME("update_username ?,?"),
    UPDATE_PASSWORD("update_password ?,?"),
    LOGOUT("update_offline ?");
    
    private final String sp_request;

    private Request(String sp_request) {
        this.sp_request = "sp_" + sp_request;
    }
    public String SP(){
        return this.sp_request;
    }
}
