
package ObjectsSerialization;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Contains guest properties, extends from player
 * Contains a function that find the MAC address
 * 
 * @author EliavBuskila
 */
public class Guest extends Player {
    
    private String MAC;

    public Guest() {
        super();
        this.MAC = null;
    }

    public String getMAC() {
        return this.MAC;
    }
    public void setMAC() {
        InetAddress ip;
        try {	
            ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            byte[] mac = network.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            
            for (int i = 0; i < mac.length; i++) {
		sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));		
            }
            this.MAC = sb.toString();
	} catch (SocketException | UnknownHostException e) {	} 
    }
    
    @Override
    public String toString() {
        return super.toString() + 
                ", MAC: " + this.MAC;
    }    
}
