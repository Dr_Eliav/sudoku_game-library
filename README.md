# Sudoku_Game-Library

to run the project: [DB & JAR](https://drive.google.com/open?id=1XHB0ZjQVd8ji0j_bJSWzf4ZQHqFWFVhM)

***SQL:***

add file -  *‘db.back’* to **SqlServer-2014**

***JAVA:***

add JAR - *‘activation.jar’* & *‘mail.jar’* & *‘mssql-jdbc-7.4.1.jre8.jar’* to **SudokuGameServer**

add **Sudoku_Game-Library** to [**Sudoku_Game-Client**](https://gitlab.com/Dr_Eliav/sudoku_game-client.git) & [**Sudoku_Game-server**](https://gitlab.com/Dr_Eliav/sudoku_game-server.git)
