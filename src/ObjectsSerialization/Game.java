
package ObjectsSerialization;

import java.io.Serializable;

/**
 * Contains game properties including user and board.
 * 
 * @author EliavBuskila
 */
public class Game implements Serializable{
    
    private int gameID;
    private Player player;
    private Board board;
    private final int[] saveBoard[];
    private long gameTime;
    private int numberOfMistakes;
    private boolean isSolved;

    public Game() {
        this.gameID = 0;
        this.player = new Player();
        this.board = new Board();
        this.saveBoard = new int[Board.SIZE][Board.SIZE];
        this.gameTime = 0;
        this.numberOfMistakes = 0;
        this.isSolved = false;
    }
    
    public int getGameID() {
        return this.gameID;
    }
    public Player getPlayer() {
        return this.player;
    }
    public Board getBoard() {
        return this.board;
    }
    public String getStringSaveBoard() {
        return Board.boardToString(saveBoard);
    }
    public int getSaveBoard(int i, int j) {
        return saveBoard[i][j];
    }
    public int[][] getSaveBoard() {
        return saveBoard;
    }
    public long getGameTime() {
        return gameTime;
    }
    public String getStringGameTime() {
        int ms = (int)(this.gameTime)%1000;
        int ss = (int)(this.gameTime/(1000))%60; 
        int mm = (int)(this.gameTime/(1000*60))%60;
        int hh = (int)(this.gameTime/(1000*60*60)%24);
        return String.format("%02d:%02d:%02d.%03d", hh, mm, ss, ms);
    }
    public int getNumberOfMistakes() {
        return this.numberOfMistakes;
    }
    public boolean getIsSolved() {
        return this.isSolved;
    }
    
    public void setGameID(int gameID) {
        this.gameID = gameID;
    }
    public void setPlayer(Player player) {
        this.player = player;
    }
    public void setBoard(Board board) {
        Board.copyBoard(saveBoard, board.getStrart());
        this.board = board;
    }
    public void setSaveBoard(int i, int j, int digit) {
        this.saveBoard[i][j] = digit;
    }
    public void setSaveBoard(int[][] board) {
        Board.copyBoard(this.saveBoard, board);
    }
    public void setSaveBoard(String board) {
        Board.StringToBoard(saveBoard, board);
    }
    public void setGameTime(long gameTime) {
        this.gameTime = gameTime;
    }
    public void setNumberOfMistakes(int numberOfMistakes) {
        this.numberOfMistakes = numberOfMistakes;
    }
    public void addNumberOfMistakes(int numOfMistakes) {
        this.numberOfMistakes += numOfMistakes;
    }
    public void addNumberOfMistakes() {
        this.numberOfMistakes++;
    }
    public void setIsSolved(boolean isSolved) {
        this.isSolved = isSolved;
    }
    
    public String toString() {
        return "GameID: " + this.gameID +
                ", Player: " + this.player.toString() + 
                ", Board: " + this.board.toString() +
                ", Save board: " + Board.boardToString(saveBoard) + 
                ", Game time: " + this.getStringGameTime() +
                ", N.O.M: " + this.numberOfMistakes +
                ", Is solved: " + this.isSolved;
    }
}
