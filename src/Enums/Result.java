
package Enums;

import static Enums.Result.values;

/**
 * All the Results the server can return are here
 * 
 * @author EliavBuskila
 */
public enum Result {
    
    SUCCESS(1),
    ERROR(0),
    EXIST_USERNAME_OR_EXIST_GMAIL(-1),
    ERROR_USER_LOGIN(-2),
    ERROR_REGISTER(-3),
    ERROR_SEND_EMAIL(-10),
    ERROR_SOLUTION(-11),
    ERROR_SOLVER(-12),
    ERROR_MAKER(-13),
    ERROR_CLIENT(-100);
    
    
    private final int value;
    
    // constractor
    private Result (int value) {
        this.value = value;
    }
    // convert enum to int
    public int getValue(){
        return this.value;
    }
    
    // convert int to enum 
    public static Result getResult(int value) {
        if (value > 0) return SUCCESS;
        for (Result type : values()) {
            if (type.getValue() == value)
                return type;
        }
        return ERROR;
    }   
}