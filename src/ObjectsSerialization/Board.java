
package ObjectsSerialization;

import java.io.Serializable;

/**
 * Contains sudoku board properties,
 * and other static functions through which sudoku boards can be converted.
 * 
 * @author EliavBuskila
 */
public class Board implements Serializable {
    
    public static final int SIZE = 9;
    
    private int boardID;
    private final int[] start[];
    private final int[] solution[];
    private int numberOfSolvers;
    private String bestTime;
    private String status;
    
    public Board() {
        this.boardID = 0;
        this.start = new int[SIZE][SIZE];
        this.solution = new int[SIZE][SIZE];
        this.numberOfSolvers = 0;
        this.bestTime = null;
        this.status = null;
    }
    public Board(Board board) {
        this.boardID = board.boardID;
        this.start = new int[SIZE][SIZE];
        this.solution = new int[SIZE][SIZE];
        copyBoard(this.start, board.start);
        copyBoard(this.solution, board.solution);
        this.numberOfSolvers = board.numberOfSolvers;
        this.bestTime = board.bestTime;
        this.status = board.status;
    }
    
    public int getBoardID() {
        return boardID;
    }
    public int[][] getStrart() {
        return start;
    }
    public String getStringStart() {
        return boardToString(start);
    }
    public int getStrart(int i, int j) {
        return start[i][j];
    }
    public int[][] getSolution() {
        return solution;
    }
    public String getStringSolution() {
        return boardToString(solution);
    }
    public int getSolution(int i, int j) {
        return solution[i][j];
    }
    public int getNumberOfSolvers() {
        return numberOfSolvers;
    }
    public String getBestTime() {
        return bestTime;
    }
    public String getStatus() {
        return status;
    }
    
    public void setBoardID(int boardID) {
        this.boardID = boardID;
    }
    public void setStrart(int[][] start) {
        copyBoard(this.start, start);
    }
    public void setStrart(String start) {
        StringToBoard(this.start, start);
    }
    public void setStart(int i, int j, int digit) {
        start[i][j] = digit;
    }
    public void setSolution(int[][] solution) {
        copyBoard(this.solution, solution);
    }
    public void setSolution(String solution) {
        StringToBoard(this.solution, solution);
    }
    public void setSolution(int i, int j,int digit) {
        solution[i][j] = digit;
    }
    public void setNumberOfSolvers(int numberOfSolvers) {
        this.numberOfSolvers = numberOfSolvers;
    }
    public void setBestTime(String bestTime) {
        this.bestTime = bestTime;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    
    public static void copyBoard(int[][] board1, int[][] board2) {
        for (int i = 0; i < SIZE; i++) 
            System.arraycopy(board2[i], 0, board1[i], 0, SIZE);
    }
    public static String boardToString(int[][] board) {
        String str = "";
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                str += board[i][j];
            }
        }
        return str;
    }
    public static void StringToBoard(int[][] board, String str) {
        for (int i = 0, index = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                board[i][j] = Character.getNumericValue(str.charAt(index++));
            }
        }   
    }
    
    @Override
    public String toString() {
        return "Board ID: " +  this.boardID + 
                ", Start: " + getStringStart() + 
                ", Solution: " + getStringSolution() + 
                ", Number of solvers: " + this.numberOfSolvers +
                ", Best time: " + this.bestTime +
                ", Status: " + this.status;
    } 
}
