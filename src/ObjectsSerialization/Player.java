
package ObjectsSerialization;

import java.io.Serializable;

/**
 * Contains player characteristics
 * This object is bequeathed to the user and guest.
 * 
 * @author EliavBuskila
 */
public class Player implements Serializable{
    
    protected int ID;
    protected String Username;
    protected boolean isUser;
 
    public Player() {
        this.ID = 0;
        this.Username = null;
    }
    public Player(Player player) {
        this.ID = player.ID;
        this.Username = player.Username;
        this.isUser = player.isUser;
    }
    
    public int getID() {
        return this.ID;
    }
    public String getUsername() {
        return this.Username;
    }
    public boolean getIsUser() {
        return this.isUser;
    }
    
    public void setID(int ID) {
        this.ID = ID;
    }
    public void setUsername(String Username) {
        this.Username = Username;
    }
    public void setIsUser(boolean isUser) {
        this.isUser = isUser;
    }
    
    @Override
    public String toString() {
        return "IsUser: " + this.isUser +
                ", ID: " + this.ID + 
                ", Username: " + this.Username;
    }  
}
