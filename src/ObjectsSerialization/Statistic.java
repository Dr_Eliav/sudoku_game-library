
package ObjectsSerialization;

import java.io.Serializable;

/**
 * Contains statistics properties.
 * 
 * @author EliavBuskila
 */
public class Statistic implements Serializable{
 
    private String username;
    private String totalPlayingTime;
    private String totalSolvedGames;
    private String completion;

    public Statistic() {
        this.username = null;
        this.totalPlayingTime = null;
        this.totalSolvedGames = null;
        this.completion = null;
    }

    public String getUsername() {
        return this.username;
    }
    public String getTotalPlayingTime() {
        return this.totalPlayingTime;
    }
    public String getTotalSolvedGames() {
        return this.totalSolvedGames;
    }
    public String getCompletion() {
        return this.completion;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setTotalPlayingTime(String totalPlayingTime) {
        this.totalPlayingTime = totalPlayingTime;
    }
    public void setTotalSolvedGames(String totalSolvedGames) {
        this.totalSolvedGames = totalSolvedGames;
    }
    public void setCompletion(String completion) {
        this.completion = completion;
    }

    @Override
    public String toString() {
        return "Username: " + this.username + 
                ", T.P.T: " + this.totalPlayingTime +
                ", T.S.G: " + this.totalSolvedGames +
                ", Copmletion: " + this.completion;
    }
}
