
package ObjectsSerialization;

import java.io.Serializable;
import Enums.*;
import java.util.Arrays;

/**
 * This object contains the information that ranges 
 * from server to client through the socket
 * 
 * Request - A constant symbolizing a request
 * Result - A constant symbolizing a result
 * info - Array of objects that should be sent
 * 
 * @author EliavBuskila
 */

public class Information implements Serializable {
    
    private Request request;
    private Result result;
    private Object[] info;

    // constructors3
    public Information() {
        this.request = null;
        this.request = null;
        this.info = null;
    }
    public Information(Request request,Result result, Object ... obj) {
        this.request = request;
        this.result = result;
        this.info = new Object[obj.length];
        System.arraycopy(obj, 0, this.info, 0, obj.length);
    }
    //--
    // getters
    public Request getRequest() {
        return this.request;
    }
    public Result getResult() {
        return this.result;
    }
    public Object getInfo(){
        return this.info[0]; // When there is only one object 
    }
    public Object getInfo(int index){
        return this.info[index];
    }
    //--
    // setters
    public void setRequest(Request request) {
        this.request = request;
    }
    public void setResult(Result result) {
        this.result = result;
    }
    public void setInfo(int index, Object obj){
        this.info[index] = obj;
    }
    //--
    
    public String toString(){
        String str = this.request+", "+this.result+", "+Arrays.toString(this.info);
        return "Information["+str+"]";
    }
}
